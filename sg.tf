module "sg_db" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "Conduit_DB_sg"
  description = "Complete PostgreSQL example security group"
  vpc_id      = module.vpc.vpc_id

  # ingress
  computed_ingress_with_source_security_group_id = [
    {
      from_port                = 5432
      to_port                  = 5432
      protocol                 = "tcp"
      description              = "PostgreSQL access from within API"
      source_security_group_id = module.sg_api.security_group_id
    },
  ]

  number_of_computed_ingress_with_source_security_group_id = 1


  computed_egress_with_source_security_group_id = [
    {
      from_port                = 5432
      to_port                  = 5432
      protocol                 = "tcp"
      description              = "api access to db"
      source_security_group_id = module.sg_api.security_group_id
    },
  ]

  number_of_computed_egress_with_source_security_group_id = 1



  tags = {
    Terraform   = "true"
    Environment = "prod"
  }
}



module "sg_alb" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "Conduit_ALB_sg"
  description = "ALB access from internet"
  vpc_id      = module.vpc.vpc_id

  # ingress
  ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "ALB acces from internet"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "ALB acces from internet"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  
  egress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "ALB acces to front"
      cidr_blocks = "0.0.0.0/0"
    },
  ]  

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }
}



module "sg_front" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "Conduit_Front_sg"
  description = "front access from alb"
  vpc_id      = module.vpc.vpc_id

  # ingress
  computed_ingress_with_source_security_group_id = [
    {
      from_port                = 80
      to_port                  = 80
      protocol                 = "tcp"
      description              = "Front access from alb"
      source_security_group_id = module.sg_alb.security_group_id
    },
  ]
  number_of_computed_ingress_with_source_security_group_id = 1

  computed_egress_with_source_security_group_id = [
    {
      from_port                = 8080
      to_port                  = 8080
      protocol                 = "tcp"
      description              = "Front access to internal alb"
      source_security_group_id = module.sg_alb_internal.security_group_id
    },
  ]

  number_of_computed_egress_with_source_security_group_id = 1



  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

}






module "sg_alb_internal" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "Conduit_ALB_internal_sg"
  description = "internal ALB access from Front"
  vpc_id      = module.vpc.vpc_id

  # ingress
  computed_ingress_with_source_security_group_id = [
    {
      from_port                = 8080
      to_port                  = 8080
      protocol                 = "tcp"
      description              = "internal ALB access from Front"
      source_security_group_id = module.sg_front.security_group_id
    },

  ]
  number_of_computed_ingress_with_source_security_group_id = 1

  computed_egress_with_source_security_group_id = [
    {
      from_port                = 8080
      to_port                  = 8080
      protocol                 = "tcp"
      description              = "Internal alb acces to api"
      source_security_group_id = module.sg_api.security_group_id
    },
  ]

  number_of_computed_egress_with_source_security_group_id = 1


  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

}


module "sg_api" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "Conduit_API_sg"
  description = "API access from internal alb"
  vpc_id      = module.vpc.vpc_id

  # ingress
  computed_ingress_with_source_security_group_id = [
    {
      from_port                = 8080
      to_port                  = 8080
      protocol                 = "tcp"
      description              = "Front access from alb"
      source_security_group_id = module.sg_alb_internal.security_group_id
    },
  ]
  number_of_computed_ingress_with_source_security_group_id = 1

  computed_egress_with_source_security_group_id = [
    {
      from_port                = 5432
      to_port                  = 5432
      protocol                 = "tcp"
      description              = "api access to db"
      source_security_group_id = module.sg_db.security_group_id
    },
  ]

  number_of_computed_egress_with_source_security_group_id = 1

    egress_with_cidr_blocks = [
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "api access to secret menager"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  
  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

}



