module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  domain_name  = var.domain_name
  zone_id      = var.zone_id
  subject_alternative_names = [
    "*.${var.domain_name}",
  ]

  wait_for_validation = true

  tags = var.tags
}

resource "aws_route53_record" "myRecord" {
  zone_id = var.zone_id
  name    = var.dns_record_name
  type    = "CNAME"
  ttl     = 60
  records = [module.alb.lb_dns_name]
}
