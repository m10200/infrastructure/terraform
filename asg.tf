data "aws_ami" "conduit_front" {
  most_recent = true
  owners      = ["self"]

  filter {
    name = "name"

    values = [
      "Conduit-Front-*",
    ]
  }
}

data "aws_ami" "conduit_api" {
  most_recent = true
  owners      = ["self"]

  filter {
    name = "name"

    values = [
      "Conduit-api-*",
    ]
  }
}





module "asg_front" {
  source  = "terraform-aws-modules/autoscaling/aws"

  # Autoscaling group
  name = "front-asg"

  min_size                  = 2
  max_size                  = 3
  desired_capacity          = 2
  wait_for_capacity_timeout = 0
  health_check_type         = "ELB"
  vpc_zone_identifier       = module.vpc.private_subnets

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      checkpoint_delay       = 600
      checkpoint_percentages = [35, 70, 100]
      instance_warmup        = 300
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  # Launch template
  launch_template_name        = "conduit-front-asg"
  launch_template_description = "Launch template for front"
  update_default_version      = true

  image_id          = data.aws_ami.conduit_front.id
  instance_type     = var.front_instance_type
  ebs_optimized     = false
  enable_monitoring = true

  security_groups          = [module.sg_front.security_group_id]
  target_group_arns        = module.alb.target_group_arns


  block_device_mappings = [
    {
      # Root volume
      device_name = "/dev/xvda"
      no_device   = 0
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 8
        volume_type           = "gp2"
      }
      }
  ]

  metadata_options = {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 32
  }

  
  tags = var.tags



  scaling_policies = {
    my-policy = {
      policy_type               = "TargetTrackingScaling"
      target_tracking_configuration = {
        predefined_metric_specification = {
          predefined_metric_type = "ASGAverageCPUUtilization"
        }
        target_value = 1.0
      }
    }
  }




}



module "asg_api" {
  source  = "terraform-aws-modules/autoscaling/aws"

  # Autoscaling group
  name = "api-asg"

  min_size                  = 1
  max_size                  = 3
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  health_check_type         = "ELB"
  vpc_zone_identifier       = module.vpc.private_subnets

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      checkpoint_delay       = 600
      checkpoint_percentages = [35, 70, 100]
      instance_warmup        = 300
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  # Launch template
  launch_template_name        = "conduit-api-asg"
  launch_template_description = "Launch template for api"
  update_default_version      = true

  image_id          = data.aws_ami.conduit_api.id
  instance_type     = var.api_instance_type
  ebs_optimized     = false
  enable_monitoring = true

  iam_instance_profile_arn = aws_iam_instance_profile.db_secret_inst_profile.arn

  security_groups          = [module.sg_api.security_group_id]

  target_group_arns = module.alb_internal.target_group_arns


  block_device_mappings = [
    {
      # Root volume
      device_name = "/dev/xvda"
      no_device   = 0
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 8
        volume_type           = "gp2"
      }
      }
  ]

  metadata_options = {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 32
  }

  
  tags = var.tags
  



  scaling_policies = {
    my-policy = {
      policy_type               = "TargetTrackingScaling"
      target_tracking_configuration = {
        predefined_metric_specification = {
          predefined_metric_type = "ASGAverageCPUUtilization"
        }
        target_value = 1.0
      }
    }
  }




}



