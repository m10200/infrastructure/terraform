resource "aws_iam_policy" "db_secret_policy" {
  name        = "db_secret_acces_policy"
  path        = "/"
  description = "acces to secret menager for api to get db password"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "secretsmanager:GetResourcePolicy",
          "secretsmanager:GetSecretValue",
          "secretsmanager:DescribeSecret",
          "secretsmanager:ListSecretVersionIds"
        ]
        Effect   = "Allow"
        Resource = [aws_secretsmanager_secret.password.arn]
      },
      {
        Action = [
          "secretsmanager:ListSecrets",
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}



resource "aws_iam_role" "db_secret_role" {
  name                = "db_secret_role"
  managed_policy_arns = [aws_iam_policy.db_secret_policy.arn]
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Effect = "Allow",
        Sid    = ""
      }
    ]
  })  
}


resource "aws_iam_instance_profile" "db_secret_inst_profile" {
  name = "db_secret_inst_prof"
  role = aws_iam_role.db_secret_role.name
}
