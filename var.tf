variable "domain_name" {
    type    = string
    default = "krzepkowsky.pl"
}

variable "zone_id" {
    type    = string
    default = "Z034802113XHL11X891O4"
  
}

variable "tags" {
    type    = map
    default = {
        Environment = "prod-conduit"
    }
}

variable "dns_record_name" {
    type = string
    default = "conduit.krzepkowsky.pl"
  
}

variable "alb_if_name" {
    type = string
    default = "conduit-if-alb"
  
}

variable "alb_internal_name" {
    type = string
    default = "conduit-internal-alb"
  
}

variable "api_instance_type" {
    type = string
    default = "t2.micro"
  
}

variable "front_instance_type" {
    type = string
    default = "t2.micro"
  
}

variable "db_instance_type" {
    type = string
    default = "db.t3.micro"
  
}

variable "db_identifier" {
    type = string
    default = "conduit"
  
}

variable "db_name" {
    type = string
    default = "conduitPostgresql"
  
}

variable "db_username" {
    type = string
    default = "conduit_postgresql"
  
}

variable "db_port" {
    type = number
    default = 5432
}

variable "db_password_secret_name" {
    type = string
    default = "db_conduit"
  
}

variable "vpc_name" {
    type = string
    default = "Conduit-vpc"
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
  
}

variable "vpc_azs" {
    type = list
    default = ["eu-central-1a", "eu-central-1b"]
}

variable "vpc_private_subnets" {
    type = list
    default = ["10.0.1.0/24", "10.0.2.0/24"]
  
}

variable "vpc_public_subnets" {
    type = list
    default = ["10.0.101.0/24", "10.0.102.0/24"]
  
}
